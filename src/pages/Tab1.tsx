import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonItem, IonIcon, IonLabel, IonButton, IonCol, IonGrid, IonRow, IonList, IonListHeader} from '@ionic/react';
import { pin, wifi, wine, warning, walk } from 'ionicons/icons';
import BookPop from '../components/BookPop';
import './Tab1.css';

const Tab1: React.FC = () => {
  var userName = localStorage.getItem('name');
  var greetingArray = [
    'Hola, ',
    'Welcome, ',
    'Privet, ',
    'Salut, ',
    'Whats up, ',
    'Hey, ',
    'Ola, ',
    '💓 '
];
var randomNumber = Math.floor(Math.random()*greetingArray.length);

var greeting = greetingArray[randomNumber];

var submessageArray = [
    'I\'m ready for a fantastic day, how about you?',
    'It\'s reading time, eh?',
    'You\'re looking good today! Let\'s read something!',
    'You are an awesome person, happy to see you!',
    'I am so happy to see you today!',
    'Did you do something new with your hair? I love it.',
    'Thanks for checking in on me, I am great!',
    'You are a special kind of awesome.',
    'Don\'t tell anyone, but you are my favourite reader',
    'Let\'s read another book, I have something great for you. 😃',
    'I have a book for you, if it was any more enjoyable it\'s title would just be '+userName+'.',
    'Wine? I think it is wine time...'
];
var randomNumbersub = Math.floor(Math.random()*submessageArray.length);

var submessage = submessageArray[randomNumbersub];
  return (
    <IonPage>
      <IonContent>

      <div className="highlight">
          <h1>{greeting} {userName}!</h1>
          <p>{submessage}</p>
          <strong>Here are the top books and stories just for you</strong>
        </div>
       

      <IonList>
          <IonListHeader>
            <IonLabel>New Music</IonLabel>
          </IonListHeader>
          <IonGrid>
            <IonRow>

                <IonCol>
                  <IonItem lines="none">
                    <IonLabel>
                      <h3>Test</h3>
                      <p>dfalfkdjalkjflkaljf</p>
                    </IonLabel>
                  </IonItem>
                </IonCol>



            </IonRow>
          </IonGrid>
        </IonList>

      </IonContent>
    </IonPage>
  );
};

export default Tab1;
