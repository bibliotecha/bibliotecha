import React, { useState } from "react";
import {
  IonImg,
  IonGrid,
  IonRow,
  IonSlides,
  IonSlide,
  IonContent,
  IonInput,
  IonItem,
  IonButton,
} from "@ionic/react";
import "./Onboard.css";

import slide1img from "../images/undraw_loving_it_y27c.svg";

var crypto = require("crypto");
var shasum = crypto.createHash("sha1");


//
// Optional parameters to pass to the swiper instance.
// See http://idangero.us/swiper/api/ for valid options.

const slideOpts = {
  initialSlide: 0,
  speed: 400,
};

const Onboard: React.FC = () => {
  const initial: string[] = [];
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [books, setBooks] = useState(initial);

  const submit = () => {
    fetch(`http://dummy.restapiexample.com/api/v1/create`, {
      body: JSON.stringify({ name, email, books: JSON.stringify(books) }),
      headers: { "Content-Type": "application/json" },
      method: "post",
    })
      .then((response: Response) => {
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        shasum.update(email + name);
        var userHash = shasum.digest('base64');
        localStorage.setItem("userHash", userHash);
        localStorage.setItem("books", JSON.stringify(books));
        console.log(response.text());
        window.location.href = '/tab1';
      })
      .catch((e) => console.log(e));
  };

  const updateBook = (e: HTMLInputElement) => {
    if (e.checked) {
      setBooks([...books, e.value]);
    } else {
      setBooks(books.filter((book) => book !== e.value));
    }
  };

  return (
    <IonContent>
      <IonSlides pager={true} options={slideOpts}>
        <IonSlide>
          <IonGrid>
            <IonRow>
              <IonImg src={slide1img} className="slideImage" />
            </IonRow>
            <IonRow>
              <div className="highlight">
                <h1>¡Hola Amigo!</h1>
                <p>
                  What should we call you? Also what's your email address?{" "}
                  <strong>
                    I am only asking to generate you a unique code to identify
                    you with, this doesn't get shared
                  </strong>
                </p>
              </div>
            </IonRow>
            <div className="swipeRight">Swipe Right</div>
          </IonGrid>
        </IonSlide>
        <IonSlide>
          <IonGrid>
            <IonRow>
              <IonItem>
                {/* TextInputWithFocusButton(); */}
                <IonInput
                  placeholder="Your First Name"
                  value={name}
                  onIonChange={(e) =>
                    setName((e.target as HTMLInputElement).value)
                  }
                ></IonInput>
                <IonInput
                  placeholder="Email Address"
                  value={email}
                  onIonChange={(e) =>
                    setEmail((e.target as HTMLInputElement).value)
                  }
                ></IonInput>
              </IonItem>
            </IonRow>
            <IonRow>
              <div className="highlight">
                <p>Don't worry this is just between us!</p>
              </div>
            </IonRow>
          </IonGrid>

          <div className="swipeRight">Swipe Right</div>
        </IonSlide>

        <IonSlide>
          <div className="highlight forceBullshit">
            <h1>What do you read?</h1>
            <p>
              I want to find you the best books, so you should let me know what
              you like!
            </p>
          </div>

          <ul className="ks-cboxtags">
            <li>
              <input
                type="checkbox"
                id="romance"
                defaultValue="romance"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="romance">romance</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="contemporary"
                defaultValue="contemporary"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="contemporary">contemporary</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="suspense"
                defaultValue="suspense"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="suspense">suspense</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="historical"
                defaultValue="historical"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="historical">historical</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="fantasy"
                defaultValue="fantasy"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="fantasy">fantasy</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="mystery-detective"
                defaultValue="mystery-detective"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="mystery-detective">mystery/detective</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="mystery"
                defaultValue="mystery"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="mystery">mystery</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="thrillers"
                defaultValue="thrillers"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="thrillers">thrillers</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="literature-fiction"
                defaultValue="literature-fiction"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="literature-fiction">literature-fiction</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="juvenile-fiction"
                defaultValue="juvenile-fiction"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="juvenile-fiction">juvenile-fiction</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="science-fiction"
                defaultValue="science-fiction"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="science-fiction">scifi</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="thriller"
                defaultValue="thriller"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />

              <label htmlFor="thriller">thriller</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="erotica"
                defaultValue="erotica"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="erotica">erotica</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="paranormal"
                defaultValue="paranormal"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="paranormal">paranormal</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="action-adventure"
                defaultValue="action-adventure"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="action-adventure">action-adventure</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="crime"
                defaultValue="crime"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="crime">crime</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="literary"
                defaultValue="literary"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="literary">literary</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="contemporary-women"
                defaultValue="contemporary-women"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="contemporary-women">contemporary-women</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="adult"
                defaultValue="adult"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />

              <label htmlFor="adult">adult</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="history"
                defaultValue="history"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="history">history</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="women-sleuths"
                defaultValue="women-sleuths"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="women-sleuths">women-sleuths</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="young-adult"
                defaultValue="young-adult"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="young-adult">ya</label>
            </li>
            <li>
              <input
                type="checkbox"
                id="horror"
                defaultValue="horror"
                onChange={(e) => updateBook(e.target as HTMLInputElement)}
              />
              <label htmlFor="horror">horror</label>
            </li>
          </ul>

          <div className="swipeRight">
            <IonButton expand="full" fill="outline" onClick={() => submit()}>
              Let's Go!
            </IonButton>
          </div>
        </IonSlide>
      </IonSlides>
    </IonContent>
  );
};

export default Onboard;
