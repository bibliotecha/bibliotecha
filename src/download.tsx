import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
var now=Date.now();
var nowString = now.toString();

function getBookList() {
    let fileTransfer = FileTransfer.create();
    const dataDirectory = 'json/';
    const url = 'http://6fgyvxswcbhxltponzzeauimlhossegc2vwp2bxwer2jkfqgrjx5rnyd.onion.com.de/api/booklist/current_compressed.json';
    fileTransfer.download(url, dataDirectory + 'current_compressed.json').then((entry) => {
        console.log('download complete: ' + entry.toURL());
        // MAKE TOAST SAYING "LATEST VERSION GRABBED"
    }, (error) => {
        // handle error
        alert(error);
    });
}
//Check if there is a newer hash than what we are storing in LocalStorage
function checkLatestBooklist(){
    var lastUpdateBooklist = localStorage.getItem('lastUpdateBooklist');
    var request = new XMLHttpRequest();
    request.open('GET', 'http://6fgyvxswcbhxltponzzeauimlhossegc2vwp2bxwer2jkfqgrjx5rnyd.onion.com.de/api/booklist/latest', true);

    request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
        // Success!
        if(this.response != lastUpdateBooklist){
            localStorage.setItem('lastUpdateBooklist',this.response); //now the current hash is correct
            localStorage.setItem('lastUpdateBook_timer', nowString);
            getBookList(); //download the latest list

        }
    } else {
        // We reached our target server, but it returned an error
        // MAKE A TOAST HERE SAYING FAILED. 
    }
    };

    request.onerror = function() {
    // There was a connection error of some sort
    };

    request.send();	
}


var lastChecked = parseInt(localStorage.getItem('lastUpdateBook_timer')!) + 1;
var lastCheckedMath = now - lastChecked;
if( lastChecked > 1209600) {
    checkLatestBooklist();
}