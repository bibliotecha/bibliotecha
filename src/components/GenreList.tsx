import React, { useState } from 'react';
//import {genres} from '../helpers/genres.js';
import TheGenreList from './GenreBookList';
import { IonList, IonItem, IonLabel, IonAvatar, IonInput, IonToggle, IonRadio, IonCheckbox, IonItemSliding, IonItemOption, IonItemOptions, IonContent, useIonViewWillEnter } from '@ionic/react';
interface Genre {
    name: string;
  }
const GenreList = () => {  
    const [genre, setGenre] = useState<Genre[]>([]);
    useIonViewWillEnter(async () => {
        const result = await fetch('https://6fgyvxswcbhxltponzzeauimlhossegc2vwp2bxwer2jkfqgrjx5rnyd.onion.com.de/api/genres.json', {
          headers: {  }
        });
        const data = await result.json();
        setGenre(data);
      });
    //var genres = ['fiction', 'romance', 'general', 'contemporary', 'suspense', 'historical', 'fantasy', 'mystery-detective', 'mystery', 'thrillers', 'literature-fiction', 'juvenile-fiction', 'science-fiction', 'thriller', 'erotica', 'paranormal', 'action-adventure', 'crime', 'literary', 'contemporary-women', 'adult', 'history', 'women-sleuths', 'young-adult', 'horror'];
    const items = []

    
    return (
            <IonList>
                {/* {genres.map((value, index) => {
                    return <IonItem key={value}>{value}</IonItem>
                })} */}
                {genre.map((genre, idx) => <EmployeeItem key={idx} genre={genre} />)}
            </IonList>
    )
};
const EmployeeItem: React.FC<{ genre: Genre }> = ({ genre }) => {
    return (
      <IonItem >
        <IonLabel>
          <h2>{genre.name}</h2>
        </IonLabel>
      </IonItem>
    );
  }
export default GenreList;
