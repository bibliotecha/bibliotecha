import React, { useState } from 'react';
//import {thegenres} from '../helpers/thegenres.js';
import { IonList, IonItem, IonLabel, IonAvatar, IonInput, IonToggle, IonRadio, IonCheckbox, IonItemSliding, IonItemOption, IonItemOptions, IonContent, useIonViewWillEnter } from '@ionic/react';
interface TheGenre {
    content: string;
    title: string;
    author: string;
    meta: string;
  }
  interface GenreProps {
    genreName: string;
  }
  const TheGenreList: React.FC<GenreProps> = ({ genreName }) => {
      const [thegenre, setTheGenre] = useState<TheGenre[]>([]);
    useIonViewWillEnter(async () => {
        const result = await fetch("https://6fgyvxswcbhxltponzzeauimlhossegc2vwp2bxwer2jkfqgrjx5rnyd.onion.com.de/api/tag/"+genreName+".json", {
          headers: {  }
        });
        const data = await result.json();
        setTheGenre(data.bookList);
      });
      
    return (
            <IonList>
                {thegenre.map((thegenre, idx) => <BookItem key={idx} thegenre={thegenre} />)}
            </IonList>
    )
};
const BookItem: React.FC<{ thegenre: TheGenre }> = ({ thegenre }) => {
    return (
      <IonItem >
        <IonLabel>
          <h2>{thegenre.title}</h2>
          <p>{thegenre.author}</p>
        </IonLabel>
      </IonItem>
    );
  }
export default TheGenreList;
