import React from 'react';
import './ExploreContainer.css';

interface ContainerProps {
  bookhash: string;
}
const BookPop: React.FC<ContainerProps> = ({ bookhash }) => {
 //GET http://6fgyvxswcbhxltponzzeauimlhossegc2vwp2bxwer2jkfqgrjx5rnyd.onion.com.de/download.php?bookhash=bookhash
  return (
    <div className="container">
      <strong>TITLE</strong> by AUTHOR
      <p>CONTENT</p>
      Tags: TAGS
    </div>
  );
};

export default BookPop;
