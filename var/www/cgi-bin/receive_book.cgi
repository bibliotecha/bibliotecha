#!/usr/bin/env python
# -*- coding:utf-8 -*-

import cgi, cgitb; 
from lxml import etree
import os, datetime, shlex, subprocess

cgitb.enable()
referer = os.environ["HTTP_REFERER"]
# SCRIPT: stores files, sends user notification, runs subprocess to include filename and metadata into catalog
print "HTTP/1.0 200 OK"
print "Content-type: text/html"
print

html="""
<!DOCTYPE html>
<html xml:lang="en" lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../lib/style_other_pages.css" type="text/css" media="screen" />
    <link href="../imgs/favicon.ico" rel="icon" type="image/x-icon" />
    <title>Bibliotecha | Upload</title>
 </head>
  <body>
<div id = 'response'>
<h2>Bibliotecha</h2>
<div>
{0}
</div>
<br/>
<p><a href="""+referer+""">Upload another book</a></p>
</div>
</body>
</html>
"""
calibre_lib = '/opt/bibliotecha/bookcase/'
tmp_bookcase_path='/opt/bibliotecha/tmp_bookcase/'

tmp_catalog_path = '/opt/bibliotecha/tmp_bookcase/'


def edit_catalog(this_tree, this_root, this_title, this_author, this_file, path, catalog):
    now = (datetime.datetime.now()).isoformat()
    newbook=etree.SubElement(this_root, "book")
    newbook.set('date', now)
    etree.SubElement(newbook, 'title').text = this_title
    etree.SubElement(newbook, 'author').text = this_author
    etree.SubElement(newbook, 'file').text = path+this_file
    this_tree.write(path+catalog, pretty_print=True, encoding='utf-8')

def create_catalog(path, catalog):
    root = etree.Element(catalog) # create xml
    tree = etree.ElementTree(root) 
    tree.write( path+catalog, pretty_print=True) # save to file
    edit_catalog(tree, root, title, authors, filename, path, catalog)

def open_catalog(path, catalog):
    parser = etree.XMLParser(remove_blank_text=True) #to ensure pretty_print of edited file
    tree = etree.parse( path+catalog, parser)
    root = tree.getroot()
    edit_catalog(tree, root, title, authors, filename, path, catalog)



# Form vars
form = cgi.FieldStorage()
title = (form.getvalue("title",  "")).decode('utf-8')
author1 = (form.getvalue("author1",  "")).decode('utf-8')
author2 = (form.getvalue("author2",  "")).decode('utf-8') 
author3 = (form.getvalue("author3",  "")).decode('utf-8')

# script vars
tmp_upload_path = "/opt/bibliotecha/tmp_bookcase/"
cgi_path = "/var/www/cgi/"

authors = author1 # authors for calibre. Structure: 1stName Surname, 1stName Surname, 1stName Surname
if author2 != "":
   authors = authors + ", " + author2
if author3 != "":
   authors = authors + ", " + author3

book =  form["book"]# form.getvalue("book", "")
filename = (book.filename).decode('utf-8')

# save the book
if book.filename: # # Test if the file was uploaded   
   fn = os.path.basename(book.filename)#return the final component of a pathname
   ext = os.path.splitext(fn)[1]#return the extension

   if ext in [".pdf", ".epub", ".mobi", ".txt", ".djvu"]:
      tmp_file = tmp_upload_path + fn
      bookfile =open(tmp_file, 'wb')
      bookfile.write((book.file).read()) #write file to disk

      # # add file to calibre         
      addbook = 'calibredb add --library-path={path} --title="{title}", --authors="{author}"  "{book}"'.format(book=tmp_file, path=calibre_lib, title=title, author=authors)
      addbook_list = shlex.split(addbook)
      devnull = open(os.devnull, 'w')
      subprocess.call(addbook, shell=True, stdout=devnull, stderr=devnull)
      os.remove(tmp_file)

      
      print html.format( ("<p>Thank you for your contribution</p><p><strong>{0}</strong> by <strong>{1}</strong> was added to Bibliotecha's collection.</p><p><a href='../collection/mobile?num=100&search=&sort=date&order=descending' title='Browse the collection'>Browse the collection</a>.</p>").format((filename).encode('utf-8'), (authors).encode('utf-8')  ) )

   else:
      print html.format("<p>It seems like the file what you are trying to upload is not a book.<br/> Bibliotecha only accepts pdf, epub, mobi, txt, djvu file formats. Please try again.</p>")

else:
   print html.format("<p>It was not possible to add your submission to Bibliotecha's catalog</p><p>Please try again or contact your local librarian.</p>")
