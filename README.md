Bibliotecha is an underground, local, and offline digital network for the distribution of electronic publications within a small communities.

Bibliotecha relies on a small computer (RaspberryPi) running open-source software to serve books over a local WIFI hotspot. Using the browser to connect to the library one can retrieve or donate texts. Bibliotecha proposes an alternative model of distribution of digital texts that allows specific communities to form and share their own collections.

# More on Bibliotecha

Bibliotecha website [http://bibliotecha.info/](http://bibliotecha.info/)

Bibliotecha Git repository [https://gitlab.com/bibliotecha/bibliotecha](https://gitlab.com/bibliotecha/bibliotecha)

Bibliotecha installation manual [https://gitlab.com/bibliotecha/bibliotecha/wikis/Manual](https://gitlab.com/bibliotecha/bibliotecha/wikis/Manual)

Bibliotecha wiki <https://gitlab.com/bibliotecha/bibliotecha/wikis/home> where you can find more techinal information and recipes.

Bibliotecha image gallery [http://bibliotecha.info/imgs/](http://bibliotecha.info/imgs/)



# External Links to Bibliotecha

Post-Digital Publishing Archive [http://p-dpa.net/work/bibliotecha/](http://p-dpa.net/work/bibliotecha/)

Video of presentation at Off the Press conference [https://vimeo.com/97508728](https://vimeo.com/97508728)
